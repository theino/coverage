// 1. Initialize
console.log("program starting");
const PORT = 3000;
const HOST = "127.0.0.1";
//const getTrafficHexColor = require('./traffic-light');
// 2. Operate
    const getTrafficHexColor = (word) => {
        if (word == "GREEN") {
            return "#00FF00";
        } else if (word == "YELLOW") {
            return "#FFFF00";
        } else if (word == "RED") {
            return "#FF0000";
        } else {
            return undefined;
        }
    };
console.log(getTrafficHexColor("RED"));
console.log(getTrafficHexColor("YELLOW"));
console.log(getTrafficHexColor("GREEN"));
console.log(getTrafficHexColor("PURPLE"));


module.exports = { getTrafficHexColor };
// 3. Cleanup
console.log('program ending');