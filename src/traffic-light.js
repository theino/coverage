// src/traffic-light.js
const getTrafficHexColor = (word) => {
    if (word == "GREEN") {
        return "#00FF00";
    } else if (word == "YELLOW") {
        return "#FFFF00";
    } else if (word == "RED") {
        return "#FF0000";
    } else {
        return undefined;
    }
};

// var red = [];
// var yellow = [];
// var green = [];

// const lightChange = async(color) => {
//     if (color == "GREEN") {
//         var green = ["FF0000", "FFFF00", null];
//         console.log(green);
//         await setTimeout(3000);
//         var green = [null, null, "#00FF00"];
//         console.log(green);
//         return green;
//     } else {
//         return false;
//     }
// };

module.exports = { getTrafficHexColor, /*lightChange*/};
