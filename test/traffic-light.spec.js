// test/traffic-light.spec.js
const expect = require('chai').expect;
const { getTrafficHexColor } = require("../src/traffic-light");
const { lightChange } = require("../src/traffic-light");
var green = require("../src/traffic-light");
const PORT = 3000;
const HOST = "127.0.0.1";
const baseUrl = `http://${HOST}:${PORT}`;


describe("Traffic lights", () => {
    it("Test traffic light possibilities", () => {
        expect(true).to.equal(true);
        expect(getTrafficHexColor("GREEN")).to.equal("#00FF00");
        expect(getTrafficHexColor("YELLOW")).to.equal("#FFFF00");
        expect(getTrafficHexColor("RED")).to.equal("#FF0000");
        // expect(getTrafficHexColor("Blue")).to.equal("#0000FF");
        expect(getTrafficHexColor("PURPLE")).to.equal(undefined);
    });
    // it("Test light color change", async() => {
    //     //expect(lightChange("GREEN")).to.equal("[null, null, #00FF00]");
    //     console.log(lightChange.green);
    //     console.log(await setTimeout(lightChange.green, 4000));
    // })
})